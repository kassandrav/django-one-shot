from django.urls import path
from . import views

urlpatterns = [
    path("", views.todo_list, name="todo_list"),
    path("<int:id>", views.show_todo, name="show_todo"),
    path("create/", views.create_todo, name="create_todo"),
    path("<int:id>/edit/", views.update_todo, name="update_todo"),
    path("<int:id>/delete/", views.delete_todo, name="delete.todo"),
    path("items/create/", views.create_item, name="create_item"),
]
