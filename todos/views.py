from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoForm, ItemForm

# Create your views here.

def todo_list(request):
    todolist = TodoList.objects.all()
    context = {
        "todo_list": todolist,
    }
    return render(request, "list.html", context)


def show_todo(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo_list,
    }
    return render(request, "detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list")
    else:
        form = TodoForm()
    context = {
        "form": form
    }
    return render(request, "create.html", context)

def update_todo(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect("show_todo", id=todolist.id)
    else:
        form = TodoForm(instance=todolist)
    context = {
        "form": form,
        "list_object": todolist,
    }
    return render(request, "edit.html", context)

def delete_todo(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list")
    else:
        return render(request, "delete.html")

def create_item(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("show_todo", id=list.id)
        else:
            form = ItemForm()
        context = {
            "form": form
        }
        return render(request, "create_item.html", context)
